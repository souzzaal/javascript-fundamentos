/*
	O objeto console fornece acesso ao console de debug

	fonte: https://developer.mozilla.org/pt-BR/docs/Web/API/Console
*/


// Saídas mais comuns
console.log('Saída comum')
console.warn('Saída na cor AMARELA e icone de alerta, no browser')
console.error('Saída na cor VERMELHA e icone de erro, no browser')
console.log('%cSaída customizada, no browser', 'color: blue; font-size:20px;')


// Saída em formato tabular
console.log('\n')
console.table(['Laranja', 'Uva', 'Acerola', 'Melancia'])
console.table([{ nome: 'Maça' }, { nome: 'Morango' }, { nome: 'Tangerina' }])

// Agrupamento de logs
console.log('\n')
console.group('NomeDoGrupo')
console.group('A')
console.log('Log agrupado e aninhado')
console.log('Log agrupado e aninhado')
console.groupEnd('A')

console.group('B')
console.log('Logs do grupo B')
console.log('Logs do grupo B')
console.groupEnd('B')
console.groupEnd('NomeDoGrupo')

// Checagem
console.log('\n')
console.assert(1 === 2, 'Se você está vendo isso, o assert falhou...') 	// forma facilitada de imprimir logs em condições específicas

// Rastreamento
console.log('\n')
console.trace('Informa o local de onde foi disparado')


// Cálculo de tempo
console.log('\n')
console.time('tempoDeExecucao')
setTimeout(() => {
	console.timeEnd('tempoDeExecucao')
}, 2000);


