// Define um array
let frutas = ["Maça", "Uva", "Laranja"]
console.log(frutas)

// Adiciona item no final do array
frutas.push("Goiaba")
console.log("\nAdicionando item ao final > ", frutas)

// Remove ultimo item do array
frutas.pop()
console.log("\nRemovendo ultimo item > ", frutas)

// Iteracao
frutas.forEach(fruta => console.log("\nItenando com forEach > ", fruta))

// Conversao do array em string
console.log("\nArray convertido em String (separado com virgula) > ", frutas.toString())
console.log("\nArray convertido em String (separado com caracter especifico) > ", frutas.join(" - "))

// Ordenacao 
console.log("\nAplicando ordencao inversa à insercao >", frutas.reverse())

// Funções que aplicam imutabilidade 
let numeros = [1, 2, 4, 5, 6]
console.log("\nNumeros >", numeros)

// Map: mapeia todos os elementos de um array para outro conjunto de valores
console.log("\nUsando MAP para calcular o dobro de cada numero >", numeros.map(numero => numero * 2))

// Filter: seleciona elementos com base em alguma condição
console.log("\nUsando FILTER para obter todos numeros pares >", numeros.filter(numero => numero % 2 == 0))

// Reduce: retora um valor cumulativo ou concatenado sobre os valores do array
console.log("\nUsando REDUCE para obter o somatorio de todos os numeros >", numeros.reduce((somatorio, numero) => somatorio
	+ numero))

// Every: verifica se todos os elementos do array atendem a uma determinada regra
console.log("\nUsando EVERY para verificar se todos os numeros são positivos > ", numeros.every(numero => numero > 0))

// Some: verifica se pelo menos um dos elementos atendem a uma determinada regra
console.log("\nUsando SOME para verificar se, pelo menos um, é maior que 5 > ", numeros.some(numero => numero > 5))

// Find: retorna o primero elemento do array que atende a uma determinada regra
console.log("\nUsando FIND para obter o primeiro numero cujo quadrado seja maior que 10 >", numeros.find(numero => numero * numero > 10))

// Includes: verifica se um array possui um elemento
console.log("\nUsando INCLUDES para verficar se o array possui o valor 1 >", numeros.includes(1))
console.log("\nUsando INCLUDES para verficar se o array possui o valor 10 >", numeros.includes(10))

