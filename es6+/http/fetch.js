

/*

	A API Fetch fornece uma interface JavaScript para acessar e manipular partes do pipeline HTTP, tais como os
	pedidos e respostas. Ela também fornece o método global fetch() que fornece uma maneira fácil e lógica para
	buscar recursos de forma assíncrona através da rede.

	Este tipo de funcionalidade era obtida anteriormente utilizando XMLHttpRequest.

	fonte: https://developer.mozilla.org/pt-BR/docs/Web/API/Fetch_API/Using_Fetch

	
	Obs: Testes no browser, node não tem implementação nativa para o fetch.
*/

let url = "https://my-json-server.typicode.com/souzzaal/fake-rest-server/users";

// Requisição default - GET
fetch(url)
	.then(responseStream => {
		console.log("responseStream", responseStream);
		return responseStream.json()	// os dados obtidos podem ser convertidos para json, text, blob...
	})
	.then(responseData => setTimeout(console.log("ResponseData", responseData), 100)) // usando setTimeout para sincronizar as saídas no console



// Fetch não acusa erro para status de resposta 4xx e 5xx
fetch(`${url}/0`)
	.then(responseStream => {
		return responseStream.json()	// os dados obtidos podem ser convertidos para json, text, blob...
	})
	.then(responseData => setTimeout(console.log("Fetch 404", responseData), 1000))
	.catch(err => congole.log("Erro " + err))


// Tratando status de resposta 
fetch(`${url}/0`)
	.then(responseStream => {
		if (responseStream.status === 200) {
			return responseStream.json()
		} else {
			// throw new Error(`response status ${responseStream.status}`)		// equivalente
			return Promise.reject(`response status ${responseStream.status}`)
		}
	})
	.then(responseData => console.log(responseData))
	.catch(err => setTimeout(console.log("CATCH " + err), 1500))


// Configurando cabeçalhos e métodos

const myHeaders = new Headers();
myHeaders.append("Content-Type", "text/json");

const body = {
	name: 'New User'
}

const configRequest = {
	method: 'POST',
	headers: myHeaders,
	body: body,
	mode: 'cors',
	cache: 'default'
};

fetch(url, configRequest)
	.then(responseStream => responseStream.json())
	.then(responseData => setTimeout(console.log("POST", responseData), 3000))