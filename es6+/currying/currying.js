/*
	Uma função concencional que calcula o dobro
*/

function calculaSoma(a, b) {
	return a + b
}


console.log("calculaSoma(2,1)", calculaSoma(2, 1));
console.log("calculaSoma(2,2)", calculaSoma(2, 2));
console.log("calculaSoma(2,3)", calculaSoma(2, 3));

/*
	Currying é uma técnica de programação funcional.

	Currying é o processo de transformar uma função que espera vários argumentos 
	em uma função que espera um único argumento e retorna outra função curried.

*/

function somar(a) {
	return function (b) {
		return a + b;
	};
}

/*
	Ao executar
		"var doisMais = somar(2)"
	é retornada para a variável doisMais uma função, que guardará estaticamente o valor 2 (graças às closures) 
	e o utilizará (tal como se estivesse definido dentro dela) para realização do cálculo.

*/

var doisMais = somar(2);
console.log("somar(2) ", doisMais);
console.log("doisMais(1) ", doisMais(1));
console.log("doisMais(2) ", doisMais(2));
console.log("somar(2)(1) " + somar(2)(1));