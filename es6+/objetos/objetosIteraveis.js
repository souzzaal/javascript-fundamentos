
/*
	O protocolo iterável permite que objetos JavaScript definam ou personalizem seu comportamento de iteração, 
	como valores em um loop do construtor for..of. Alguns tipos built-in são built-in iterables com um comportamento 
	de iteração padrão, tal como Array ou Map, enquanto outros tipos (como Object) não são assim.

	Para ser iterável, um objeto deve implementar o método @@iterator, o que significa que o objeto (ou um dos objetos 
	acima de sua cadeia de protótipos) deve ter uma propriedade com uma chave @@iterator que está disponível via 
	constante Symbol.iterator.

	fonte: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Iteration_protocols

	
	A seguir, exemplos de como construir objetos iteráveis.

*/

/* 
	 Forma manual - construindo a função iteradora 

	Passos:
	1 - Criar uma propriedade computada com o operador Symbol, com isso o objeto passa a ter uma interface de iteração.
	2 - Desenvolver o que será devolvido a cada chamda de 'iterador.next()', por isso o retorno deve ser uma função 
		chamada 'next'. Em geral os iteradores retornam um objeto, contendo duas propriedades:
			value: o dado iterado a ser devolvido
			done: um booleano que indica o fim da lista de dados
*/
const obj = {
	values: [1, 2, 3, 4],

	[Symbol.iterator]() {
		let indice = 0;
		return {
			next: () => {
				return {
					value: this.values[indice++],
					done: indice > this.values.length
				}
			}
		}
	}
}

// obtendo iterador
const it = obj[Symbol.iterator]()

// iterando
while (true) {
	item = it.next();
	if (item.done) {
		break
	}
	console.log("while + iterador:", item)
}


/*
	Forma simplificada - usando generators

	Pode-se utlizar as funções geradoras (generators) para criar a função iteradora. Para isso, adicionamos um asterisco 
	antes do nome da propriedade computada gerada com o operador Symbol.

*/
const obj2 = {
	values: [1, 2, 3, 4, 5],

	*[Symbol.iterator]() {
		for (let i = 0; i < this.values.length; i++) {
			yield this.values[i]
		}
	}
}

for (let valor of obj2) {
	console.log("for of: ", valor);
}

// objetos iteráveis permite o uso de spread
const arr = [...obj]
console.log("Spread com objeto iterável:", arr);
