
/*
	Forma classica de se definir um objeto literal
*/

let obj = {
	propriedade: 'prop',
	metodo: function () { }
}
console.log(obj);

/*
	Poderiam ser atribuido também usando "propriedades e métodos" externos
*/

var propriedade = "conteudo"
function metodo() { }

obj = {
	propriedade: propriedade,
	metodo: metodo
}
console.log(obj);

/*
	A partir do ES6, caso a(s) propriedade(s) ou método(s) tenha(m) o mesmo(s) nome(s)
	pode-se utilizar essa forma compacta de escrita, omitindo o lado direito
*/

obj = {
	propriedade,
	metodo
}
console.log(obj);


/*
	Funções também podem ser esctritas desta forma
*/

obj = {
	metodo(arg) {
		console.log(arg);
	}
}
obj.metodo("shorthand")


/*
	Criando objetos com proriedades computadas
*/

propName = 'name'
// antes do es6 era precisa criar o objeto primeiro, em seguida atribuir a propridade:
oldObj = {}
oldObj[propName] = 'André'


// com o es6+ pode-se fazer diratamente
obj = {
	[propName]: 'André'
}
console.log(obj, oldObj);
