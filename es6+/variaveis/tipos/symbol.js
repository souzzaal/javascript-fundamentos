/*
	Permite criar identificadores únicos

	Symbol não pode ser instanciado com 'new', deve ser chamado como se fosse uma função.
	Symbol aceita um argumento, que serve apenas para efeito de identicação.
	Symbol permite a criação de objetos iteráveis.

*/

// Demais tipos 
const str1 = "André";
const str2 = "André";
console.log("str1 é igual a str2? ", str1 === str2); // conteudo e tipo são iguais?


// Symbols são únicos
const symbol1 = Symbol("André");
const symbol2 = Symbol("André");
console.log("symbol1 é igual a symbol2 ", symbol1 === symbol2); // conteudo e tipo são iguais?


/*
	Previne conflito entre nomes de propriedades 
	O código abaixo imprime um objeto com apenas uma propriedade name, mas duas lastname.
*/
const nameString1 = "name";
const nameString2 = "name";
const lastNameSymbol1 = Symbol("lastname");
const lastNameSymbol2 = Symbol("lastname");
const user = {
	[nameString1]: "Pedro",
	[nameString2]: "Paulo",
	[lastNameSymbol1]: "Pedeira",
	[lastNameSymbol2]: "Paulada",
	age: 0
};
console.log("user tem um name dois lastnames: ", user, user.lastNameSymbol1);

/* 
	Propriedades Symbols não são enumeráveis 
	
	Propriedades enumeráveis são aquelas propriedades cuja flag interna [[Enumerable]] é verdadeira (true), 
	que é o padrão para propriedades criadas via assinatura simples ou através de um inicializador.
 
	Propriedades enumeráveis aparecem em for...in loops exceto se o nome da propriedade for um objeto Symbol.

	fonte: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Enumerabilidade_e_posse_de_propriedades

*/
for (const key in user) {
	if (user.hasOwnProperty(key)) {
		console.log(`Valor da chave ${key}: ${user[key]}`);
	}
}
console.log("Propriedades do objeto user:", Object.keys(user));
console.log("Valores das propriedades do objeto user:", Object.values(user));


// Exibir symbols de um objeto
console.log(
	"Symbols registrados no objeto user:",
	Object.getOwnPropertySymbols(user)
);

// Acessando todas as propriedades do objeto
console.log("Todas propriedades do objeto user:", Reflect.ownKeys(user));

// Criar um enum
const directions = {
	UP: Symbol("UP"),
	DOWN: Symbol("DOWN"),
	LEFT: Symbol("LEFT"),
	RIGHT: Symbol("RIGHT")
};
console.log("directions.UP", directions.UP)
