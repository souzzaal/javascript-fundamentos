
// escopo global
const variaveis = [];
var escopoGlobal = "global"

{
	// escopo de bloco

	/*
	
		let e const respeitam escopo de bloco, var não (apenas global e função)!
		let e const não sofrem hoisting
	
	*/
	var varEscopoBloco = 'var escopo bloco'
	let letEscopoBloco = "só existe a partir desta linha"
	variaveis.push(new Variavel('global', 'var', 'dentro de bloco', varExists(escopoGlobal)))
	variaveis.push(new Variavel('bloco', 'var', 'dentro de bloco', varExists(varEscopoBloco)))
	variaveis.push(new Variavel('bloco', 'let', 'dentro de bloco', varExists(letEscopoBloco)))

	try {
		varExists(hoisting)
	} catch (error) {
		variaveis.push(new Variavel(' - ', 'const', 'qualquer escopo usando hoisting', false))
	}
	const hoisting = 'só existe a partir desta linha';

}
variaveis.push(new Variavel('bloco', 'var', 'fora de bloco', varExists(varEscopoBloco)))

try {
	varExists(letEscopoBloco)
} catch (error) {
	variaveis.push(new Variavel('bloco', 'let', 'fora de bloco', false))
}

function testEscopo() {
	//escopo de função
	var escopoFuncao = "função";

	variaveis.push(new Variavel('global', 'var', 'dentro de função', varExists(escopoGlobal)))
	variaveis.push(new Variavel('bloco', 'var', 'dentro de função', varExists(varEscopoBloco)))
	try {
		varExists(letEscopoBloco)
	} catch (error) {
		variaveis.push(new Variavel('bloco', 'let', 'dentro de função', false))
	}
	variaveis.push(new Variavel('função', 'var', 'dentro de função', varExists(escopoFuncao)))
}
testEscopo();

try {
	varExists(escopoFuncao)
} catch (error) {
	variaveis.push(new Variavel('função', 'var', 'fora de função', false))
}

console.table(variaveis)


function varExists(variavel) {
	return typeof variavel !== 'undefined' ? true : false;
}

function Variavel(escopo, tipo, atuacao, existe) {
	this.escopo = escopo;
	this.tipo = tipo;
	this.atuacao = atuacao;
	this.existe = existe;
}