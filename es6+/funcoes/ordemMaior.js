/*

	Significa que uma função pode ser passada como parâmetro ou retrnada por uma função

*/


function getNome() {
	return "André";
}

// função passada como argumento
function myLog(fn) {
	console.log("====> " + fn());
}
myLog(getNome)


// função returnada por outra função
function retornaGetNome() {
	return getNome
}

var funcaoGetNome = retornaGetNome()
console.log(funcaoGetNome())