
/*
	Promise é um objeto usado para processamento assíncrono. Um Promise (de "promessa") representa um valor
	que pode estar disponível agora, no futuro ou nunca.

	Um Promise representa um proxy para um valor que não é necessariamente conhecido quando a promessa é criada.
	Isso permite a associação de métodos de tratamento para eventos da ação assíncrona num caso eventual de sucesso
	ou de falha. Isto permite que métodos assíncronos retornem valores como métodos síncronos: ao invés do valor final,
	o método assíncrono retorna uma promessa ao valor em algum momento no futuro.

	Um Promise está em um destes estados:

		- pending (pendente): Estado inicial, que não foi realizada nem rejeitada.
		- fulfilled (realizada): sucesso na operação.
		- rejected (rejeitado):  falha na operação.
		- settled (estabelecida): Que foi realizada ou rejeitada.

	fonte: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Promise
*/


/*
	Criando promise

	Uma promisse é criada usando 'new Promise()' e deve receber como argumento uma outra função, que por sua vez,
	deve teve os argumentos resolve e reject.

	Resolve deve ser chamada quando o processamento foi bem sucessido e esta retornará seu resultado.
	Reject deve ser chamada quando o processamento não foi bem sucedido e retornará o erro.

	Exemplos: https://www.luiztools.com.br/post/programacao-assincrona-em-nodejs-callbacks-e-promises/
*/


function soAceitaPares(numero) {
	const promise = new Promise((resolve, reject) => {
		if (numero % 2 === 0) {
			resolve(numero);							// se o numero for par resolve será chamado
		}
		else {
			reject("Você passou um número ímpar!"); 	// se o numero for impar reject será chamado
		}
	});
	return promise;
}


/*
	Consumindo promises

	Como o código a ser excutado/avaliado pela promisse é assincrono, devemos usar a método then(), para que,
	somente quando a promisse estiver retornado, possamos tomar a ação seguinte se forma segura.

	Caso a promisse seja rejeitada, ou algum erro seja disparado, podemos usar o método catch() para processar
	o erro.
*/

soAceitaPares(2)
	.then(sucesso => console.log(sucesso));

soAceitaPares(3)
	.then(sucesso => console.log(sucesso))
	.catch(erro => console.log(erro))


// Esta sintaxe também pode ser utilizada para criação de uma promise
function dividePelaMetade(numero) {
	if (numero % 2 !== 0)
		return Promise.reject(new Error("Não posso dividir um número ímpar!"));
	else
		return Promise.resolve(numero / 2);
}

/*
	Promises podem ser encadeadas por meio do then().

	Ao "empilhar" promises, cada promisse da pilha só será executada quando a promise que estiver acima dela
	for finalizada. Isso torna simples o tratamento de recursos assincronos. 

	Ao usar promises o processamento é feito de forma assincrona, observe os logs das chamadas abaixo
*/


soAceitaPares(10)
	.then(resultadoSoAceitaPares => dividePelaMetade(resultadoSoAceitaPares))
	.then(resultadoDividePelaMetade => console.log("O valor de soAceitaPares foi " + (resultadoDividePelaMetade * 2)))
	.catch(err => console.log(err))


soAceitaPares(11)
	.then(resultadoSoAceitaPares => dividePelaMetade(resultadoSoAceitaPares))
	.then(resultadoDividePelaMetade => console.log("O valor de soAceitaPares foi " + (resultadoDividePelaMetade * 2)))
	.catch(err => console.log("ERRO encadeado: " + err))

soAceitaPares(10)
	.then(resultadoSoAceitaPares => dividePelaMetade(resultadoSoAceitaPares + 1)) // forçando o reject na 2ª promisse
	.then(resultadoDividePelaMetade => console.log("O valor de soAceitaPares foi " + (resultadoDividePelaMetade * 2)))
	.catch(err => console.log("ERRO encadeado: " + err))


/*
	Pode-se utilizar o Promise.all() para executar diversas promises independentes de forma paralela, podendo
	executar alguma ação somente após a conclusão de todas elas. Caso algum erro seja disparado ou alguma
	promise rejeitada, todo o fluxo é interrompido e pode ser tratado com um catch().

*/

Promise.all([soAceitaPares(10), dividePelaMetade(4)])
	.then(resultadoDeTodasAsPromises => {
		console.log("Resultado de soAceitaPares:" + resultadoDeTodasAsPromises[0])
		console.log("Resultado de dividePelaMetade:" + resultadoDeTodasAsPromises[1])
	})

/*
	Promise.race() é pareceida com Promise.all(), mas retorna apenas uma promise: que resolver ou rejeitar primeiro.
*/


var promiseLenta = new Promise(function (resolve, reject) {
	setTimeout(resolve, 500, "lenta");
});
var promiseRapida = new Promise(function (resolve, reject) {
	setTimeout(resolve, 100, "rápida");
});

Promise.race([promiseLenta, promiseRapida]).then(function (value) {
	console.log("Promise.race:", value);
	// Ambos resolvem, mas promiseRapida é mais rápida
});
