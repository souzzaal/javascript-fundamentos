/*

	Default values permite definir values padrões para argumentos de função

*/

function soma(a, b) {
	return a + b
}
console.log("argumentos obrigatórios", soma(2, 2));


/*
	Define valores padrões para a e b
*/
function soma2(a = 0, b = 0) {
	return a + b
}
console.log("argumentos opcionais", soma2());


/*
	O valor de b será igual ao valor de a
	Atenção: a variável a ser atribuida precisa estar declarada. Neste exemplo a declaração soma3(a = b, b) gera erro
*/
function soma3(a = 0, b = a) {
	return a + b
}
console.log("b = a", soma2(5));


const random = () => parseInt(Math.random() * 10)
/*
	O argumento pode receber uma função (lazy evaluation)
*/
function soma4(a = 0, b = random()) {
	return a + b
}
console.log("argumento recebe função", soma4());