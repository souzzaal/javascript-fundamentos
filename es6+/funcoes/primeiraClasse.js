/*
	Siginifica que uma função pode ser atribuida a uma variável, objeto ou array.

*/

// variável
var somar = function (a, b) {
	return a + b
};
console.log("Somar 2+3 ", somar(2, 3));

// objeto
var objeto = {
	nome: "Andre"
};

objeto.getNome = function () {
	return this.nome;
}
console.log("Meu nome é:", objeto.getNome());

//array
var calculadora = [
	function (a, b) { return a + b },
	function (a, b) { return a - b },
]
console.log("calculadora[0](1,2)", calculadora[0](1, 2));
console.log("calculadora[1](1,2)", calculadora[1](1, 2));

