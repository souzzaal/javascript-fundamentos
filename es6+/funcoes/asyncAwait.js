/*
	ES7+

	É uma forma facilitada (também conhecido como açucar sintático) de se trablhar com Promises.

	Desta forma para "criar" uma promise basta adicionar a palavra "async" na declaração da função:

		async function funcaoClassica() {}

		const arrowFunction = async () => {}

	No corpo da função ao usar "return" a promise é resolvida. Caso algum erro seja disparado, a promise
	é rejeitada.
*/

async function funcaoClassica() {
	return 'O retorno de uma função declarada com async é uma Promise.'
}
console.log(funcaoClassica());

const arrowFunction = async () => '0 - Como dito antes, retorno de uma função declarada com async é uma Promise.'
arrowFunction()
	.then(resposta => console.log(resposta.concat(" Logo podemos encadear then e catch.")))

/*
	Já a palavra "await" é usada em conjunto com as chamadas das funções "async" e tem por objetivo
	fazer com que o script pause sua execução enquanto aguarda a finalização da função assincona chamda
	(que retorna uma promise, logo await aguarda a conclusão da promise).

	Await só pode ser utlizado dentro de funções assíncronas.

	No exemplo abaixo, a função usaAwait() faz uma chamada para uma função assíncrona, que, por sua vez, 
	chama outras funções assíncronas e ao observar o log, vemos que os números 1, 2, 3 e 4 são apresentados
	em ordem, como se fossem chamadas de funções sincronas. O número 0, fora de ordem, é o resultado da 
	chamada de arrowFunction(),	onde não foi utilizado o await. 

	Async/Await torna simples o uso de chamadas assíncronas (simplificando o uso de promises) e facilitando
	a leitura e manutenção de código, pois embora seja assíncrono, a estrutura é muito semelhante com a
	utilização de funções/chamadas sincronas. 
*/

async function asyncFunc() {

	const fazAlgumaCoisaAssincrona = async () => {
		const fazAlgumaOutraCoisaAssincrona = async () => {
			return await funcaoClassica()
		}
		return await fazAlgumaOutraCoisaAssincrona()
	}

	console.log('2 - antes do encadeamento assíncrono maluco')
	const resultado = await fazAlgumaCoisaAssincrona()
	console.log('3 - depois do encadeamento assíncrono maluco')
	return resultado
}

async function usaAwait() {
	console.log("\n1 - Antes do await")
	const retorno = await asyncFunc()	// await pausa a execução até que se tenha o retorno da função
	console.log("4 - Esperei todo o processamento do encadeamento, mas já sei que... ", retorno)
}
usaAwait()


