
/*
	Generators (funções geradoras) são funções que permitem que seu fluxo de execução sejam pausados até que 
	um evento de iteração ocorra. Generators usam iteradores para percorrer os 'passos' da função.

	Para criar uma função geradora é preciso usar um * após a palavra function.
	Através de um objeto iterador, cada vez que next() é chamdo, a função é executada até encontrar a palavra
	chave yield.

*/


// yield retorna um valor
function* hello() {
	yield 'Hello'
	yield 'Generators'
}

// obtendo iterador
let it = hello()
console.log(it.next()) 	// { value: 'Hello', done: false }
console.log(it.next())	// { value: 'Generators', done: false }
console.log(it.next()) 	// { value: undefined, done: true }

// yield tambem pode receber um argumento, que pode ser passado na chamda da função next().
function* hello2() {
	const name = yield 'Hello';	// retorna 'Hello' e guarda o valor passado, na próxima chamada de next() em 'name'
	yield name;
}
const it2 = hello2()
console.log('\n\nFunções geradoras com parametros')
console.log(it2.next()) 			// { value: 'Hello', done: false }
console.log(it2.next('André')) 		// { value: 'André', done: false }
console.log(it2.next())				// { value: undefined, done: true }	


// Ex. criando um função que retorna um número incrementado a cada chamda
function* proximo(inicio = 0) {
	while (true) {					// impede o fim da contagem
		yield inicio++;
	}
}
const it3 = proximo();
console.log('\n\nContador')
console.log(it3.next())
console.log(it3.next())

const it4 = proximo(1000);
for (let i = 0; i < 10; i++) {
	console.log(it4.next());
}