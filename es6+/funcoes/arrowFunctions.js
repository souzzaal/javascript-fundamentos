/*
	Arrow functions são funcões anonimas, ou seja, só podem ser utilizadas sendo atribuidas a uma variável ou
	como argumento de uma função.

	Obrigatóriamente tem que usar "=>".

	Arrow function não sofrem hoisting.

*/

// Função anônima convencional
let somaTradicional = function (a, b) {
	return a + b;
}
console.log("Função tradicional:", somaTradicional(1, 2));


/* 
	Arrow function - simples de uma linha apenas 
	A palacra chave return não é necessária, pois o retorno é implícito
*/
var soma = (a, b) => a + b
console.log("Arrow funcion:", soma(1, 2));


/* 
	Arrow function - multiplas linhas
	É obrigatório o {} para o corpo da função.
	Neste caso o return deve ser explicitado, quando necessário.
*/
var soma = (a, b) => {
	return a + b
}
console.log("Arrow funcion:", soma(1, 2));


/*
	Arrow function com um agrumento apenas
	Pode-se omitir os parêntesis.
	Para mais de um argumento os parêntesis são obrigatórios.
*/
log = arg => console.log(arg);
log("argumento único")

/*
	Arrow function com rest operator
	Obriagatório o usuo de parêntesis.
*/
log = (...rest) => console.log(rest);
log("arg1", 2, [3])

/*
	Arrow function default values
	Obriagatório o usuo de parêntesis.
*/
log = (a = 5) => console.log(a);
log() // 5

/*
	Arrow functions com objetos literais ()
	Obriagatório o usuo de parêntesis, no lado direito da declaração
*/
let createPessoa = () => ({ nome: "", idade: 0 })
log(createPessoa())


/*
	Arrow funtions e o THIS

	A palavra-chave this dentro de uma arrow function respeita o contxto léxico, isto é, ela nunca extrapolará

	Exemplo: passo-a-passo

	1 - Um objeto comum do JS, esse código funciona porque as funções guardam o contexto de invovação (closure)
*/

let obj = {
	showContext() {
		this.log(this) 	// o contexto do this é o contexto do objeto, isto é, o this aponta para o próprio objeto
	},
	log(arg) {
		console.log("contexto lexico", arg);
	}
}
obj.showContext()

/*
	2 - Problema: exisem funções do JS em que o contexto léxico não é respeitado, pois são invocadas no contexto global
*/

obj = {
	showContext() {
		// this.log(this) 								// log é reconhecido aqui

		setTimeout(function () {
			// this.log(this);							// Erro: log não é reconhecido aqui 
			console.log("problema de contexto", this)	// Aqui o this não faz referência para obj
		}, 1000)
	},
	log(arg) {
		console.log("problema de contexto", arg);
	}
}
obj.showContext()

/*
	3 - Uma solução é usar o método bind ou apply para forçar o contexto

*/
obj = {
	showContext() {
		//this.log(this) 					// log é reconhecido aqui

		setTimeout(function () {
			this.log(this);					// log também é reconhecido aqui 
		}.bind(this), 1000)					// bind força o contexto da função anônima
	},
	log(arg) {
		console.log("solucao bind", arg);
	}
}
obj.showContext()

/*
	3.1 - Uma solução gambiarra que pode ser encontrada

*/
obj = {
	showContext() {
		// this.log(this) 					// log é reconhecido aqui
		let self = this						// armazena o contexto

		setTimeout(function () {
			self.log(self);					// log também é reconhecido aqui 
		}, 1000)
	},
	log(arg) {
		console.log("solucao self", arg);
	}
}
obj.showContext()

/*
	4 - Solução com arrow functions
	Arrow functions respeitam o contexto de criação

*/
obj = {
	showContext() {
		// this.log(this) 							// log é reconhecido aqui

		setTimeout(() => {
			this.log(this);							// log também é reconhecido aqui 
		}, 1000)
	},
	log(arg) {
		console.log("solucao arrow function", arg);
	}
}
obj.showContext()