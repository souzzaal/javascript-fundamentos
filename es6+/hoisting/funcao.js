/*

	Hoisting significa levantar/suspender algo.

	As declarações são elevadas ao topo do escopo que em que se encontram
	(bloco, local, global),	de forma a evitar erros de variáveis/funções 
	não delcaradas. 
	
	É como se em tempo de execução o código fosse reorganizado e todas DECLARAÇÔES
	de variáveis/funções fosse reposicionadas no topo da escopo.

*/

/*

	HOISTING DE FUNÇÂO

	Toda a função é elevada ao topo do escopo, podendo ser chamada mesmo antes 
	de sua declaração (física) no código.

	Ainda que o JS permita é uma boa pratica sempre declarar a função antes 
	de utilizá-la. 

*/

function fn() {
	log("Hoisting de função")
}

fn();

function log(value) {
	console.log(value);
}
