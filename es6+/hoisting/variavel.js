/*

	Hoisting significa levantar/suspender algo.

	As declarações são elevadas ao topo do escopo que em que se encontram
	(bloco, local, global),	de forma a evitar erros de variáveis/funções 
	não delcaradas. 
	
	É como se em tempo de execução o código fosse reorganizado e todas DECLARAÇÔES
	de variáveis/funções fosse reposicionadas no topo da escopo.

*/

/*

	HOISTING DE VARIÁVEL
	
	Eleva apenas a declaração da variável ao topo do escopo, não a sua atribuição.
	
	O primerio console.log exibe "Antes da declaração undefined", ou seja, 
	o javascript sabe que a variável existe (que foi declarada), mas não 
	sabe seu valor ainda. É como se o código fosse o seguinte:

	function fn() {
		var text;

		console.log("Antes da declaração " + text)

		text = "Exemplo"

		console.log("Após a declaração " + text)
	}

*/

function fn() {

	console.log("Antes da declaração: " + text)

	var text = "conteúdo da variável"

	console.log("Após a declaração: " + text)
}

fn();