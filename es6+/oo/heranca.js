
class Veiculo {

	constructor(qtdRodas) {
		this.qtdRodas = qtdRodas
	}
}

class Carro extends Veiculo {

	constructor(qtdPortas) {
		super(4)
		this.qtdPortas = qtdPortas
	}
}

const ferrari = new Carro(4)
console.log(
	"ferrari: ", ferrari, "\n",
	"ferrari qtdRodas: ", ferrari.qtdRodas, "\n"
);