
class Veiculo {

	constructor(qtdRodas) {
		this.qtdRodas = qtdRodas
		this.ligado = false;
	}

	ligar() {
		this.ligado = true;
		return 'ligando...';
	}

	desligar() {
		this.ligado = false;
		return 'desligado...';
	}

	status() {
		return this.ligado ? 'Ligado' : 'Desligado'
	}
}

const moto = new Veiculo(2);
console.log(
	"moto: ", moto, '\n',
	"moto.ligar() ", moto.ligar(), '\n',
	'moto.status() ' + moto.status()
);


class Carro extends Veiculo {

	#qtdPortas = 0; 				// # é o modificador de acesso privado para JS
	#tetoSolar;

	constructor(qtdPortas) {
		super(4)
		this.#qtdPortas = qtdPortas
		this.#tetoSolar = false;
	}

	get qtdPortas() {
		return this.#qtdPortas
	}

	set qtdPortas(numero) {
		return this.#qtdPortas = numero
	}
}

const ferrari = new Carro(4)
console.log(
	"ferrari: ", ferrari, "\n",
	"ferrari qtdRodas: ", ferrari.qtdRodas, "\n",
	"ferrari tetoSolar: ", ferrari.tetoSolar, "\n",
);