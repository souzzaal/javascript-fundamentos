
class Veiculo {

	#qtdRodas = 0; 				// # é o modificador de acesso privado para JS
	constructor(qtdRodas) {
		this.#qtdRodas = qtdRodas
	}

	static buzina() {
		return "Biiiiiiiiiiiiiiiiiiiiiii"
	}
}

console.log("static buzina()", Veiculo.buzina())

const moto = new Veiculo(2);
console.log("acessando propridade privada", moto.qtdRodas) // undefined



class Carro extends Veiculo {

	#qtdPortas;

	constructor(qtdPortas) {
		super(4)
		this.#qtdPortas = qtdPortas
	}

	get qtdPortas() {
		return this.#qtdPortas
	}

	set qtdPortas(numero) {
		this.#qtdPortas = numero
	}
}

const ferrari = new Carro(4)
console.log("acessando propridade privada via get ", ferrari.qtdPortas)
ferrari.qtdPortas = 2; // alterando propridade privada via set 
console.log("acessando propridade privada via get ", ferrari.qtdPortas)
