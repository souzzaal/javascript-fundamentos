
/*

	Um closure (fechamento) é uma função que se "lembra" do ambiente — ou escopo léxico — em que ela foi criada.

	A função contador() retorna a função aninhada - um closure - maisUm(), que reutiliza uma variável global em relação ao 
	seu escopo. 

	Como maisUm() depende da variável contagem, esta será mantida estaticamente, por isso ao chamar contadorMaisMais(),
	é exibido o valor incrementado de contagem.

	Este é um exemplo de escopo léxico: em JavaScript, o escopo de uma variável é definido por sua localização dentro 
	do código fonte (isto é aparentemente léxico) e funções aninhadas têm acesso às variáveis declaradas em seu 
	escopo externo.

	fonte: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Guide/Closures

*/

function contador() {
	var contagem = 0

	function maisUm() {
		contagem++
		console.log("Contagem atual: " + contagem)
	}

	return maisUm
}

var contadorMaisMais = contador()


for (let i = 0; i <= 10; i++) {
	contadorMaisMais()
}