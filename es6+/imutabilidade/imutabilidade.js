
/*

	Conceito de programação funcional

	Quando estamos falando de imutabilidade em JavaScript estamos falando em evitar modificar referências.

*/


function push(array, elemento) {
	array.push(elemento)
	return array
}

function pop(array) {
	return array.pop()
}

const arr = [7, 3]
console.log("arr =>", arr)

push(arr, 12)           			// arr --> [7, 3, 12]
console.log("push(12) = arr =>", arr)

const last = pop(arr) 				// arr --> [1, 7, 3]
console.log("pop() = arr =>", arr)
console.log(last == 12)				// true


/*
	JS passa argumento por refência, isto é, as alterações são aplicadas no objeto base. 
	O que pode provocar efeitos colaterais em muitas situações.
*/


// Exemplos de aplicação manual da imutabilidade

function push2(array, elemento) {
	var copia = array.slice(0, array.length)

	copia.push(elemento)
	return copia
}
console.log("push2(12) =>", push2(arr, 12), "arr =>", arr);


function pop2(array) {
	var copia = [...array];

	copia.pop()
	return copia
}
console.log("pop2()", pop2(arr), "arr =>", arr);


// Exemplos de imutabildiade com funções nativas JS

console.log("Filter => ", arr.filter(x => x > 5), "arr =>", arr);
console.log("Map => ", arr.map(x => x * 2), "arr =>", arr);