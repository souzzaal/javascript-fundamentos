/*

	A sintaxe de atribuição via desestruturação (destructuring assignment) é uma expressão de atribuiçao do
	 JavaScript que possibilita extrair dados de arrays ou objetos em variáveis distintas.

	 A variável desestruturada não faz referência para array/objeto de onde foi gerada.

*/

titleLog = title => console.log(`\n\n == ${title} == \n`)

titleLog('ARRAYS')

var arr = ['maca', 'uva', 'morango']

// jeito antigo - verboso
var maca = arr[0]
var uva = arr[1]
var morango = arr[2]

/* 
	es6+ destructuring assignment 

	O lado esquedo da atribuição deve ter a mesma estrutura que o objeto de onde se extrai as informações (lado direito).
	Diz-se que arr foi destruído, e seus elementos destribuidos nas variáveis.

	No exemplo abaixo: cada indice de 'arr' é atribuidos nas variáveis declaradas à esquerda (é o jeito antigo sendo feito 
	de forma implicita). 
*/
var [maca2, uva2, morango2] = arr
console.log("array desestruturado: ", maca2, uva2, morango2)


// desestruturando array multidemensionais
var arr = ['maca', 'uva', 'morango', ['laranja']]
var [maca2, uva2, morango2, [laranja]] = arr
console.log("sub array desestruturado: ", laranja)


/*
	Desestruturando objeto dentro de array
	
	No exemplo abaixo: o lado esquerdo diz que arr2 tem, no indice zero, um objeto.
	Desse objeto, a propriedade 'tipo' está sendo atribuída à variável 'tipo'
*/
var arr2 = [{ nome: 'abacaxi', tipo: 'fruta' }, { nome: 'tangerina', tipo: 'fruta' }]
var [{ tipo }] = arr2
console.log("desenstruturando objeto dentro de array:", tipo);


titleLog("OBJETOS")

var obj = {
	nome: "Patriota",
	props: {
		idade: 0
	},
	coresFavoritas: ['verde', 'amarelo', 'azul', 'branco']
}

// jeito antigo
var nome0 = obj.nome

// es6+ destructuring assignment 
var { nome } = obj;
console.log("objeto desestruturado:", nome);

/* 
	Para redefinir o nome da variável usa-se " : novoNome " 
	Por padrão a variável deve ter o mesmo nome da propriedade do objeto
*/
var { nome: meuNome } = obj;
console.log("objeto desestruturado para variável pesonalizada:", meuNome);

/* 
	Desestruturando objetos aninhados 
	A expressão de desestruturação (lado esquerdo) deve ter a mesma estrutura do objeto que se deseja extrair

	No exemplo abaixo estamos extraindo a propriedade idade de 'obj' e colocando numa variável chamda 'minhaIdade'
*/

var { props: { idade: minhaIdade } } = obj;
console.log("objeto aninhado desestruturado para variável pesonalizada:", minhaIdade);


/*
	Desestruturando array dentro de objeto.
	
	No exemplo abaixo: o lado esquerdo diz que obj tem uma propriedade coresFavoritas, que é um array.
	Desse array, o indice 0 está sendo atribuído à variável verde2
*/
var { coresFavoritas: [verde2] } = obj;
console.log("desestruturando array dentro de objeto:", verde2);

titleLog('FUNÇÕES')

/*
	A desestruturação pode se aplicada nos parâmetros de função.

	No exemplo abaixo: a função soma recebe um array como argumento e retorna a soma de seus dois primeiros
	índices
*/

function soma([a, b]) {
	return a + b;
}
console.log("Destruturando argumento de função com array:", soma([1, 2]));
// soma(1, 5) Isso gera erro

/*
	Desestruturação aplicaca em conjunto com valoes padrões (default values)
*/
function soma2([a = 0, b = 0]) {
	return a + b;
}
console.log("Destruturando argumento de função com default values:", soma2([1]));


/*

	No exemplo abaixo: a função soma recebe um objeto como argumento e retorna a soma de suas propriedades a e b.
	É obrigatória a existencia das propriedades no objeto.
*/

function soma3({ a, b }) {
	return a + b;
}
console.log("Destruturando argumento de função com objeto:", soma3({ a: 2, b: 3, c: 1 }));

function soma4({ a = 0, b = 0 }) {
	return a + b;
}
console.log("Destruturando argumento de função com objeto:", soma4({ c: 1 }));



