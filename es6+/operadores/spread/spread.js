
/*

	Operador ... (Spread)

	Sintaxe de Espalhamento (Spread syntax) permite que um objeto iterável, como uma expressão de array
	ou uma string seja expandido para ser usado onde zero ou mais argumentos (para chamadas de funções)
	ou elementos (para arrays literais) são esperados, ou que um objeto seja expandido onde zero ou mais
	pares propriedade:valor (para objetos literais) são esperados.

	Muito útil para manter o conceito de imutabilidade

*/

titleLog = title => console.log(`\n\n == ${title} == \n`)

titleLog('FUNÇÕES')
// Uso em argumento de funções
function somaTresNumeros(a, b, c) {
	return a + b + c
}

console.log("chamda normal -> somaTresNumeros(1,2,3) ", somaTresNumeros(1, 2, 3))
const params = [1, 2, 3, 4]
console.log("chamda com spread -> somaTresNumeros( ...params) ", somaTresNumeros(...params));


titleLog('ARRAYS')

// Contatenando arrays
const tresQuatro = [3, 4]
const numeros = [1, 2, ...tresQuatro, 5]
console.log("numeros ", numeros);


// Copia de array
const copia = [...numeros]
console.log("copia ", copia)
console.log("copia === numeros", copia === numeros)

// Merge de arrays
const seisSete = [6, 7]
const mesclado = [...numeros, ...seisSete]
console.log("mesclado", mesclado);
// mesclado.forEach(n => console.log(n, typeof n))



titleLog('OBJETOS LITERAIS')

// Uso com objetos literais, apenas para criação de novos objetos
var obj1 = { a: 1, b: 2, c: 3, s: { prop: 'valor' } }
var obj2 = { a: 'um', b: 'dois', c: 'tres', d: 'quatro' }

// clonagem de objetos - clonagem rasa: sub-objetos manterao a referencia para o objeto original
var cloneRaso = { ...obj1 }
console.log("cloneRaso", cloneRaso);
console.log("cloneRaso === obj1", cloneRaso === obj1)
console.log("cloneRaso.s === obj1.s", cloneRaso.s === obj1.s)
// Para resolver a clonagem rasa, pode-se usar spread no sub-objeto tambem
var cloneTotal = { ...obj1, s: { ...obj1.s } }
console.log("cloneTotal === obj1", cloneTotal === obj1)
console.log("cloneTotal.s === obj1.s", cloneTotal.s === obj1.s)

// mesclagem de objetos - propriedades iguais de objetos mais à esqueda são sobrescritas
var objetoMesclado = { ...obj1, ...obj2, d: [4], e: 5 }
console.log("objetoMesclado", objetoMesclado)

// Isso não pode ser feito, pois um objeto literal não é iterável (ver criar objetos itaráveis em objetosIteraveis.js)
try {
	var arr = [...obj1]
} catch (error) {
	console.log("ERRO: " + error.message);
}