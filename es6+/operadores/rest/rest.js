
/*

	A sintaxe rest se parece exatamente como a sintaxe de espalhamento, mas esta é usada para desestruturar
	arrays e objetos. De certa forma, a sintaxe rest é o oposto da sintaxe de espalhamento: A sintaxe de espalhamento
	(spread) 'expande' um array em vários elementos, enquanto a sintaxe rest coleta multiplos elementos e 'condensa'
	eles em um único elemento.

*/


// Uso em argumento de funções 

// permite criar funcoes com numero variável de argumentos

// antes do es6 - funciona mas a legibilidade fica prejudicada
function somatorioAntigo(a, b) {
	let soma = 0;
	for (let i = 0; i < arguments.length; i++) {
		soma += arguments[i]
	}
	return soma
}
console.log("somatorioAntigo(1,2,3) ", somatorioAntigo(1, 2, 3))

// com o es6+ usando o operador rest - numeros é um array
function somatorio(...numeros) {

	return numeros.reduce(
		(soma, numero) => soma += numero	// 'soma' guarda o valor que será retornado, 'numero' referencia aos elementos do array
		, 0									// define o tipo de retorno (numero) e seu valor inicial (0)
	)
}

console.log("somatorio(1,2,3) ", somatorio(1, 2, 3))
console.log("somatorio(1,2,3,3,2,1) ", somatorio(1, 2, 3, 3, 2, 1))


// agrupa parte dos argumentos em numa variável local
function myFun(a, b, ...outros) {
	console.log("a", a)
	console.log("b", b)
	console.log("outros", outros)
}
myFun(1, 'Dois', [3], { quatro: 4 })



/*

	A sintaxe de atribuição via desestruturação (destructuring assignment) é uma expressão JavaScript que
	 possibilita extrair dados de arrays ou objetos em variáveis distintas.

*/

var a, b, rest;
[a, b] = [1, 2];
console.log(a); // 1
console.log(b); // 2

[a, b, ...rest] = [1, 2, 3, 4, 5];
console.log(a); // 1
console.log(b); // 2
console.log(rest); // [3, 4, 5]

({ a, b } = { a: 1, b: 2 });
console.log(a); // 1
console.log(b); // 2

({ a, b, ...rest } = { a: 'um', b: 'dois', c: 'tres', d: 'quatro' });
console.log(a); // um
console.log(b); // dois
console.log(rest); // { c: 'tres', d: 'quatro' }